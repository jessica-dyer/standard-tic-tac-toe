def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

def game_over(board, space):
    print_board(board)
    print("GAME OVER")
    print(board[space], "has won")
    exit()

def is_winner(board, space_one, space_two, space_three):
    return board[space_one] == board[space_two] == board[space_three]

def is_row_winner(board, row_number): # This isn't the best solution...but it's A solution!
    sequence = []
    first_term = 0
    if row_number == 1:
        first_term = 0
    elif row_number == 2:
        first_term = 3
    elif row_number == 3:
        first_term = 6

    for n in range(1,4):
        num_in_sequence = first_term + (n -1)*1 # Calculate what the space numbers are based on row number and common difference btwn terms.
        sequence.append(num_in_sequence)

    return board[sequence[0]] == board[sequence[1]] == board[sequence[2]]


board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if is_row_winner(board, 1):
        game_over(board, 0)
    elif is_row_winner(board, 2):
        game_over(board, 3)
    elif is_row_winner(board, 3):
        game_over(board, 6)
    elif is_winner(board, 0, 3, 6):
        game_over(board, 0)
    elif is_winner(board, 1, 4, 7):
        game_over(board, 1)
    elif is_winner(board, 2, 5, 8):
        game_over(board, 2)
    elif is_winner(board, 0, 4, 8):
        game_over(board, 0)
    elif is_winner(board, 2, 4, 6):
        game_over(board, 2)

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
